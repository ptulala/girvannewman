package utils

import groovy.time.TimeCategory
import groovy.time.TimeDuration


class Benchmark {
    static <V> V benchmarked(String processName, Closure<V> benchmarkedProcess, boolean indented = false) {
        print processName + (indented ? ':\n' : '... ')
        def startDate = new Date()
        def result = benchmarkedProcess()
        def endDate = new Date()
        TimeDuration duration = TimeCategory.minus(endDate, startDate)
        println indented ? "$processName done in $duration" : "[$duration]"
        result
    }
}
