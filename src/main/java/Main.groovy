import algorithm.GirvanNewman
import graph.DirectedGraph
import graph.UndirectedGraph

class Main {
    public static void main(String[] args) {
        if (args.size() == 0) {
            println 'Usage: gn [-d] <graphFile>\n'
            println 'Flags:'
            println '  -d   if graphFile is a directed graph'
            return
        }
        File graphFile = new File(args.last())

        if (!graphFile.exists()) {
            throw new Exception("File does not exist.")
        }

        def g = args.contains('-d') ? DirectedGraph.loadFromFile(graphFile) : UndirectedGraph.loadFromFile(graphFile)
        GirvanNewman alg = new GirvanNewman(g)
        def components = alg.execute()
        println "----"
        println "The following communities were found:"
        components.each { println it }
    }
}
