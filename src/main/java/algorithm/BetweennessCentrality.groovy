package algorithm

import graph.Edge
import graph.Graph
import graph.Vertex
import groovy.transform.TupleConstructor

/**
 * Implementation based on http://www.algo.uni-konstanz.de/publications/b-fabc-01.pdf
 */
class BetweennessCentrality {
    @TupleConstructor
    protected class VertexParam {
        List<Edge> shortestPathInEdges
        Integer numberOfShortestPaths
        Integer shortestPathLength
    }

    Graph graph

    BetweennessCentrality(Graph graph) {
        this.graph = graph
    }

    Map<Edge, Double> calculate() {
        Map<Edge, Double> betweenness = [:]
        graph.edges.each { betweenness.put(it, 0) }

        graph.vertices.each { Vertex startVertex ->
            Map<Vertex, VertexParam> vertexParams = [:]

            // Initialize
            graph.vertices.each { vertexParams.put(it, new VertexParam([], 0, -1)) }
            vertexParams[startVertex].with {
                numberOfShortestPaths = 1
                shortestPathLength = 0
            }

            def queue = [startVertex] as Queue<Vertex>
            def stack = [] as Stack<Vertex>

            // BFS
            while (!queue.empty) {
                def vertex = queue.remove()
                stack.push(vertex)

                vertex.outEdges.each { Edge edge ->
                    def vParams = vertexParams[vertex]
                    def wParams = vertexParams[edge.to]

                    // visiting the vertex for the first time
                    if (wParams.shortestPathLength == -1) {
                        queue.add(edge.to)
                        wParams.shortestPathLength = vParams.shortestPathLength + 1
                    }

                    // shortest path to "edge.to" via "vertex"
                    if (wParams.shortestPathLength == vParams.shortestPathLength + 1) {
                        wParams.numberOfShortestPaths += vParams.numberOfShortestPaths
                        wParams.shortestPathInEdges += edge
                    }
                }
            }

            Map<Edge,Double> dependency = [:]

            graph.edges.each { dependency.put(it, 0) }

            while (!stack.empty) {
                Vertex vertex = stack.pop()
                Double innerSum = vertex.outEdges.sum { dependency[it] } ?: 0
                vertexParams[vertex].shortestPathInEdges.each { Edge edge ->
                    dependency[edge] = (vertexParams[edge.from].numberOfShortestPaths / vertexParams[vertex].numberOfShortestPaths) * (1 + innerSum)
                    betweenness[edge] += dependency[edge]
                }
            }
        }

        betweenness
    }
}
