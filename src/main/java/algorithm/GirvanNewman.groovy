package algorithm

import graph.Graph
import graph.Vertex
import utils.Benchmark

/**
 *  Implements Girvan-Newman algorithm
 *   Input: Undirected graph
 *   Output: Communities of the given graph
 */
class GirvanNewman {
    Graph graph
    Map<Vertex, Boolean> visitedVertices
    List<Set<Vertex>> components

    GirvanNewman(Graph graph) {
        this.graph = graph.clone() as Graph
    }

    Graph iteration() {
        def betweenness
        Benchmark.benchmarked('Calculating betweenness') {
            betweenness = new BetweennessCentrality(graph).calculate()
        }
        def maxBetweenness = betweenness.max { it.value }.value
        def edgesToRemove = betweenness.findAll { (it.value as Double).round(2) == (maxBetweenness as Double).round(2) }.collect { it.key }
        edgesToRemove.each { graph.removeEdge(it) }
        graph
    }

    protected dfs(Vertex v) {
        def component = [v] as SortedSet
        v.outEdges*.to.each {
            if (!(it in visitedVertices)) {
                visitedVertices[it] = true
                component += it
                component += dfs(it)
            }
        }
        component
    }

    protected determineComponents() {
        components = []
        visitedVertices = graph.vertices.collectEntries { [it, false] }
        graph.vertices.each {
            if (!(it in visitedVertices)) {
                def component = dfs(it)
                if (!component.empty) {
                    components << component
                }
            }
        }
    }

    Set<List<Set<Vertex>>> execute() {
        def setComponents = [] as Set
        Benchmark.benchmarked('Executing GN algorithm', {
            while (!graph.edges.empty) {
                iteration()
                determineComponents()
                setComponents << components.collect { it.id }
            }
        }, true)
        setComponents
    }
}
