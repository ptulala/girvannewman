package graph

import groovy.transform.CompileStatic

@CompileStatic
abstract class Graph {
    Map<Integer, Vertex> vertices = [:]
    List<Edge> edges = []

    Vertex getAt(Integer id) {
        if (vertices.containsKey(id)) {
            return vertices[id]
        } else {
            throw new Exception("Vertex does not exist.")
        }
    }

    Vertex createVertex(Integer id) {
        def vertex = new Vertex(id)
        vertices.put(id, vertex)
        vertex
    }

    Edge createEdge(Vertex from, Vertex to) {
        def edge = new Edge(from, to)
        from.outEdges << edge
        to.inEdges << edge
        edges << edge
        edge
    }

    Collection<Vertex> getVertices() {
        vertices.values()
    }

    List<Edge> getEdges() {
        edges
    }

    def removeEdge(Edge edge) {
        edge.from.outEdges.remove(edge)
        edge.to.inEdges.remove(edge)
        edges.remove(edge)
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Graph g = this.class.newInstance() as Graph
        vertices.each { g.createVertex(it.key) }
        edges.each { g.createEdge(g[it.from.id], g[it.to.id]) }
        g
    }
}
