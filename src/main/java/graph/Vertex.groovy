package graph

import groovy.transform.CompileStatic

@CompileStatic
class Vertex implements Comparable {
    Integer id
    List<Edge> inEdges = []
    List<Edge> outEdges = []

    Vertex(Integer id) {
        this.id = id
    }

    int compareTo(Object o) {
        assert o instanceof Vertex
        id.compareTo(o.id)
    }
}
