package graph

import groovy.transform.CompileStatic
import groovy.transform.TupleConstructor

@CompileStatic
@TupleConstructor
class Edge {
    Vertex from
    Vertex to
}
