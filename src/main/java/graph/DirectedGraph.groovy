package graph

import groovy.transform.CompileStatic
import utils.Benchmark

import java.util.regex.Matcher
import java.util.regex.Pattern

@CompileStatic
class DirectedGraph extends Graph {
    static Graph loadFromFile(File inputFile) {
        DirectedGraph g = new DirectedGraph()
        Pattern linePattern = Pattern.compile('(\\d+)\\s(\\d+)')
        def vertices = [] as Set

        Benchmark.benchmarked('Loading graph') {
            inputFile.readLines().each { line ->
                Matcher m = linePattern.matcher(line)
                if (m.matches()) {
                    def from = m.group(1) as Integer
                    def to = m.group(2) as Integer
                    [from, to].each {
                        if (!vertices.contains(it)) {
                            g.createVertex(it)
                            vertices << it
                        }
                    }
                    g.createEdge(g[from], g[to])
                }
            }
        }

        g
    }
}
